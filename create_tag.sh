#!/bin/bash

VERSION=""

NAT='0|[1-9][0-9]*'
SEMVER_REGEX="\
^[vV]?\
($NAT)\\.($NAT)\\.($NAT)"

function validate_version() {
  local version=$1

  if [[ "$version" =~ $SEMVER_REGEX ]]; then
    # if a second argument is passed, store the result in var named by $2
    if [ "$#" -eq "2" ]; then
      local major=${BASH_REMATCH[1]}
      local minor=${BASH_REMATCH[2]}
      local patch=${BASH_REMATCH[3]}
      eval "$2=(\"$major\" \"$minor\" \"$patch\")"
    else
      echo "$version"
    fi
  else
    echo "regex error"
  fi
}

function bump() {
  local new
  local version
  local command

  case $# in
  2) case $1 in
    major | minor | patch)
      command=$1
      version=$2
      ;;
    *) echo "major minor or patch" ;;
    esac ;;
  *) echo "unknown command" ;;
  esac

  validate_version "$version" parts
  # shellcheck disable=SC2154

  local major="${parts[0]}"
  local minor="${parts[1]}"
  local patch="${parts[2]}"

  case "$command" in
  major) new="$((major + 1)).0.0" ;;
  minor) new="${major}.$((minor + 1)).0" ;;
  patch) new="${major}.${minor}.$((patch + 1))" ;;
  *) echo "unknown command major minor or patch" ;;
  esac

  VERSION="$new"
}

function tag_push() {
  FAILED=false
  echo "Tag Push Starting..."
  curl --fail -X POST -H "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "$GITLAB_HOST/api/v4/projects/$CI_PROJECT_ID/repository/tags?tag_name=v$1&ref=$CI_COMMIT_SHA" || FAILED=true

  if [ "$FAILED" = true ]; then
    echo "Pushing Tag Failed"
    CREATE_VERSION=false
    slack "Pushing Tag Failed"
  fi
  echo "Tag Push finished"
}

function slack() {
  slack_message="<@$GITLAB_USER_LOGIN> project=$CI_PROJECT_NAME branch=$CI_COMMIT_BRANCH info=$1 <$CI_PIPELINE_URL|link>"
  curl --location --request POST "${SLACK_HOOK}" \
    --header 'Content-type: application/json' \
    --data-raw '{
      "username": "Gitlab",
      "channel": "'"${SLACK_CHANNEL}"'",
      "text": "'"${slack_message}"'"
  }'
}

function open_merge_request() {
  BRANCH_NAME=$1
  FAILED=false
  echo "Merge Request Starting..."
  curl --fail -X POST -H "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "$GITLAB_HOST/api/v4/projects/$CI_PROJECT_ID/merge_requests?source_branch=$BRANCH_NAME&target_branch=develop&title=New-$BRANCH_NAME" || FAILED=true

  if [ "$FAILED" = true ]; then
    echo "Merge Request Failed"
    CREATE_VERSION=false
    slack "Merge Request Failed"
  fi
  echo "Merge Request Finished"
}

echo "Script Starting..."
CREATE_VERSION=false

git remote set-url origin "https://oauth2:${GITLAB_API_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
USER_NAME=$(echo "$GITLAB_USER_EMAIL" | cut -d'@' -f1)
git config user.name "$USER_NAME"
git config user.email "$GITLAB_USER_EMAIL"
git fetch || exit 1

BRANCH_NAME=$CI_COMMIT_BRANCH
echo "BRANCH_NAME:""$BRANCH_NAME"
echo "DEPLOY_TYPE:""$DEPLOY_TYPE"

if [[ -n $BRANCH_NAME == *"feature"* ]]; then
  echo "Feature-Release Starting..."
  CREATE_VERSION=false
  VERSION_BASE=$(git tag --list | tail -1 | cut -d'v' -f2)
  echo "VERSION_BASE:""$VERSION_BASE"

  if [[ -z $VERSION_BASE ]]; then
    VERSION_BASE=0.0.1
  fi

  if [[ $DEPLOY_TYPE == *"major"* ]]; then
    echo "Creating Major"
    bump major "$VERSION_BASE"
  else
    echo "Creating Minor"
    bump minor "$VERSION_BASE"
  fi
  SPLIT_VERSION_BASE=$(echo "$VERSION" | cut -c1-3)
  NEW_RELEASE=release/"$SPLIT_VERSION_BASE"
  echo "NEW_RELEASE:""$NEW_RELEASE"
  git checkout -b "$NEW_RELEASE"
  git push origin "$NEW_RELEASE"
  echo "Feature-Release Finished See New Pipeline"
elif [[ $BRANCH_NAME == *"release"* ]]; then
  echo "Release Starting..."
  PATTERN=v$(echo "$BRANCH_NAME" | cut -d'/' -f2)
  VERSION_BASE=$(git tag --list | grep "$PATTERN" | tail -1 | cut -d'v' -f2)
  TAG_INIT=false
  if [[ -z "$VERSION_BASE" ]]; then
    echo "Tag Not found(New Release)"
    TAG_INIT=true
    VERSION_BASE=$(echo "$BRANCH_NAME" | cut -d'/' -f2).0
  fi
  echo "VERSION_BASE:""$VERSION_BASE"
  if [ "$TAG_INIT" = true ]; then
    VERSION=$VERSION_BASE
  else
    echo "Patching"
    bump patch "$VERSION_BASE"
  fi

  CREATE_VERSION=true

  open_merge_request "$BRANCH_NAME"
  if [ "$CREATE_VERSION" = true ]; then
    tag_push "$VERSION"
  fi

  slack "Release Starting"

  echo "Release Finished"
fi

echo "VERSION:""$VERSION"
echo "CREATE_VERSION:""$CREATE_VERSION"

echo "$VERSION" >version
echo $CREATE_VERSION >createVersion
echo "Script Finished"
